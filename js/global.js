//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [];

//AWARDs NUMBER
var awardNum = 0;

//OXYGEN AMOUNT 
var oxygenAmount = 0,
	oxygenPointer;

var drawPointer = function()
{
	switch(oxygenAmount)
	{
		case 0: oxygenPointer.css("transform", "rotate(-75deg)");
				break;
		case 10: oxygenPointer.css("transform", "rotate(-65deg)");
				break;
		case 20: oxygenPointer.css("transform", "rotate(-50deg)");
				break;
		case 30: oxygenPointer.css("transform", "rotate(-35deg)");
				break;
		case 40: oxygenPointer.css("transform", "rotate(-20deg)");
				break;
		case 50: oxygenPointer.css("transform", "rotate(-5deg)");
				break;
		case 60: oxygenPointer.css("transform", "rotate(10deg)");
				break;
		case 70: oxygenPointer.css("transform", "rotate(25deg)");
				break;
		case 80: oxygenPointer.css("transform", "rotate(40deg)");
				break;
		case 90: oxygenPointer.css("transform", "rotate(55deg)");
				break;
		case 100: oxygenPointer.css("transform", "rotate(70deg)");
				break;
	}
}

var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 's')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<sup>" + tObj.text[tObj.currentSymbol] + "</sup>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var launch000 = function()
{
	
}

var launch101 = function()
{
	var theFrame = $("#frame-101"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-frame-button"),
		blackSkin = $(prefix + ".black-skin"),
		text = $(prefix + ".text-container"), 
		o2 = $(".o2");
	
	textType = new TypingText(text, 50, true, function(){
		timeout[0] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 5000);
	});
	
	nextButton.fadeOut(0);
	o2.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		textType.write();
		timeout[0] = setTimeout(function(){
			o2.fadeIn(0);
			timeout[1] = setTimeout(function(){
				o2.css({
					"width": "40%",
					"height": "46%",
					"left": "30%",
					"top": "25%"
				});
			}, 1000);
			
			timeout[1] = setTimeout(function(){
				oxygenAmount += 100;
				drawPointer();
			}, 3000);
			
			timeout[1] = setTimeout(function(){
				oxygenAmount -= 100;
				drawPointer();
			}, 6000);
			
			timeout[1] = setTimeout(function(){
				o2.css({
					"width": "",
					"height": "",
					"left": "",
					"top": ""
				});
			}, 8000);
		}, 5000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch102 = function()
{
	var theFrame = $("#frame-102"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		blackSkin = $(prefix + ".black-skin"),
		ship = $(prefix + ".ship");
	
	audio[0] = new Audio("audio/spacecraft-sound.mp3");
	
	audio[0].addEventListener("ended", function(){
		hideEverythingBut($("#frame-103"));
	});
	
	shipSprite = new Motio(ship[0], {
		"fps": "1.5", 
		"frames": "5"
	});
	
	shipSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}	
	});
	
	oxygenAmount = 100;
	drawPointer();
	
	ship.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.css("opacity", "0");
		audio[0].play();
		ship.fadeIn(500);
		timeout[0] = setTimeout(function(){
			blackSkin.css("opacity", "1");
			blackSkin.addClass("transition-5s");
			shipSprite.play();
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch103 = function()
{
	var theFrame = $("#frame-103"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		blackSkin = $(prefix + ".black-skin"),
		nextButton = $(prefix + ".next-button"),
		words = $(prefix + ".word"), 
		atomStructure = $(prefix + ".atom-structure-rus, "+prefix + ".atom-structure-kaz, " + prefix + ".atom-structure-eng"),
		protonNumber = $(prefix + ".proton-number-rus, " + prefix + ".proton-number-kaz, " + prefix + ".proton-number-eng"),
		isotope = $(prefix + ".isotope-rus, " + prefix + ".isotope-kaz, " + prefix + ".isotope-eng"),
		massNumber = $(prefix + ".mass-number-rus, " + prefix + ".mass-number-kaz, " + prefix + ".mass-number-eng"),
		nuclei = $(prefix + ".nuclei-rus, " + prefix + ".nuclei-kaz, " + prefix + ".nuclei-eng");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	audio[1] = new Audio("audio/s3-1.mp3");
	audio[2] = new Audio("audio/s3-2.mp3");
	audio[3] = new Audio("audio/s3-3.mp3");
	audio[4] = new Audio("audio/s3-4.mp3");
	audio[5] = new Audio("audio/s3-5.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	audio[1].addEventListener("ended", function(){
		protonNumber.css("left", "");
		timeout[0] = setTimeout(function(){
			audio[2].play();
		}, 1000);
	});
	audio[2].addEventListener("ended", function(){
		isotope.css("left", "");
		timeout[1] = setTimeout(function(){
			audio[3].play();
		}, 1000);
	});
	audio[3].addEventListener("ended", function(){
		massNumber.css("left", "");
		timeout[2] = setTimeout(function(){
			audio[4].play();
		}, 1000);
	});
	audio[4].addEventListener("ended", function(){
		nuclei.css("left", "");
		timeout[3] = setTimeout(function(){
			audio[5].play();
		}, 1000);
	});
	audio[5].addEventListener("ended", function(){
		timeout[3] = setTimeout(function(){
			audio[0].pause();
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 3000);
	});
	
	oxygenAmount -= 10;
	drawPointer();
	words.css("left", "2000px");
	nextButton.fadeOut(0);
	
	var startButtonListener = function(){
		audio[0].play();
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		words.addClass("transition-5s");
		atomStructure.css("left", "");
		timeout[0] = setTimeout(function(){
			audio[1].play();
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch104 = function()
{
	var theFrame = $("#frame-104"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		labels = $(prefix + ".labels"),
		items = $(prefix + ".item");
	
	audio[0] = new Audio("audio/s4-1.mp3");
	audio[1] = new Audio("audio/s4-2.mp3");
	
	audio[0].addEventListener("ended", function(){
		items.fadeIn(1000);
		audio[1].play();
		timeout[0] = setTimeout(function(){
			labels.fadeIn(1000);
		}, 1000);
	});
	
	audio[1].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			blackSkin.fadeIn(1000);
			nextButton.fadeIn(0);
		}, 3000);
	});
	
	nextButton.fadeOut(0);
	labels.fadeOut(0);
	items.fadeOut(0);
	
	oxygenAmount -= 10;
	drawPointer();
			
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch105 = function()
{
	var theFrame = $("#frame-105"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		label = $(prefix + ".label"),
		labelImage = $(prefix + ".label-image"),
		portrait = $(prefix + ".portrait");
	
	audio[0] = new Audio("audio/s5-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		items.fadeIn(1000);
		audio[1].play();
		timeout[0] = setTimeout(function(){
			labels.fadeIn(1000);
		}, 1000);
	});
	
	var textType = new TypingText(label, 50, true, function(){
		timeout[1] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 3000);
	});
	
	nextButton.fadeOut(0);
	label.fadeOut(0);
	portrait.fadeOut(0);
	labelImage.fadeOut(0);
	
	oxygenAmount -= 10;
	drawPointer();
			
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
		label.fadeIn(500);
		labelImage.fadeIn(1000);
		textType.write();
		timeout[0] = setTimeout(function(){
			portrait.fadeIn(500);
		}, 2000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch106 = function()
{
	var theFrame = $("#frame-106"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		cloud = $(prefix + ".s6-stroenie"),
		electrons = $(prefix + ".electrons");
	
	var electronSprite = new Motio(electrons[0], {
		"fps": "2", 
		"frames": "3"
	});
	
	audio[0] = new Audio("audio/s6_1.mp3");
	audio[1] = new Audio("audio/s6_2.mp3");
	audio[2] = new Audio("audio/s6_3.mp3");
	audio[3] = new Audio("audio/s6_4.mp3");
	
	audio[0].addEventListener("ended", function(){
		audio[1].play();
		cloud.fadeIn(1000);
	});
	audio[1].addEventListener("ended", function(){
		audio[2].play();
	});
	audio[2].addEventListener("ended", function(){
		audio[3].play();
		electronSprite.play();
	});
	audio[3].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 2000);
	});
	
	nextButton.fadeOut(0);
	cloud.fadeOut(0);
	
	oxygenAmount -= 10;
	drawPointer();
			
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch107 = function()
{
	var theFrame = $("#frame-107"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		items = $(prefix + ".item");
	
	audio[0] = new Audio("audio/s7_1.mp3");
	
	audio[0].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 3000);
	});
	
	nextButton.fadeOut(0);
	items.fadeOut(0);
	
	oxygenAmount -= 10;
	drawPointer();
			
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
		fadeOneByOne(items, 0, 1000, function(){});
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch108 = function()
{
	var theFrame = $("#frame-108"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		area = $(prefix + ".area"),
		nucleonNumber = $(prefix + ".nucleon-number"),
		nucleonNumberLabel = $(prefix + ".nucleon-number .label"),
		k = $(prefix + ".K");
	
	var areaType = new TypingText(area, 50, false, function(){
		
	}); //TypingText(jqueryElement, interval, hasSound, endFunction)
	
	var nucleonNumberSprite = new Motio(nucleonNumber[0], {
		"fps": "1",
		"frames": "4"
	});
	nucleonNumberSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			nucleonNumberLabel.css("color", "white");
			this.pause();
		}
	});
	
	nucleonNumberSprite.to(3, true);
	
	audio[0] = new Audio("audio/s8_1.mp3");
	audio[1] = new Audio("audio/s8_2.mp3");
	audio[2] = new Audio("audio/s8_3.mp3");
	audio[3] = new Audio("audio/s8_4.mp3");
	audio[4] = new Audio("audio/s8_5.mp3");
	audio[5] = new Audio("audio/s8_6.mp3");
	audio[6] = new Audio("audio/s8_7.mp3");
	
	audio[0].addEventListener("ended", function(){
		audio[1].play();
	});
	audio[1].addEventListener("ended", function(){
		audio[2].play();
	});
	audio[2].addEventListener("ended", function(){
		audio[3].play();
	});
	audio[3].addEventListener("ended", function(){
		audio[4].play();
	});
	audio[4].addEventListener("ended", function(){
		audio[5].play();
	});
	audio[5].addEventListener("ended", function(){
		audio[6].play();
		area.fadeOut(0);
		k.fadeOut(0);
		nucleonNumber.fadeIn(1000);
		nucleonNumberSprite.play();
	});
	audio[6].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 3000);
	});

	nextButton.fadeOut(0);
	nucleonNumberLabel.css("color", "transparent");
	nucleonNumber.fadeOut(0);
	area.fadeOut(0);	
	
	oxygenAmount -= 10;
	drawPointer();
			
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		area.fadeIn(500);
		areaType.write();		
		audio[0].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch109 = function()
{
	var theFrame = $("#frame-109"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		task = $(prefix + ".task"),
		text = $(prefix + ".text"),
		balls = $(prefix + ".ball"),
		periodicTable = $(prefix + ".periodic-table"),
		helpButton = $(prefix + ".help-button"),
		keepThinking = $(prefix + ".keep-thinking"),
		hint = $(prefix + ".hint");

	var taskType = new TypingText(task, 50, true, function(){
		timeout[0] = setTimeout(function(){
			balls.fadeIn(500);
			text.fadeIn(500);
			task.fadeOut(0);
		}, 1000);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("color", vegetable.css("color"));
		basket.css("font-size", vegetable.css("font-size"));
		basket.css("font-style", vegetable.css("font-style"));
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		blackSkin.fadeIn(0);
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			blackSkin.fadeOut(0);
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length <= 11;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		hint.fadeIn(1000);
		audio[4].play();
	};
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	audio[0] = new Audio("audio/alarm-sound.mp3");
	audio[1] = new Audio("audio/sci-fi.mp3");
	audio[2] = new Audio("audio/s9_1.mp3");
	audio[3] = new Audio("audio/s9_2.mp3");
	audio[4] = new Audio("audio/s11_1.mp3");
	audio[5] = new Audio("audio/s12_1.mp3");
	audio[6] = new Audio("audio/s12_2.mp3");
	audio[7] = new Audio("audio/s12_3.mp3");
	
	audio[1].addEventListener("ended", function(){
		audio[1].play();
	});	
	audio[2].addEventListener("ended", function(){
		audio[3].play();
	});
	audio[3].addEventListener("ended", function(){
		taskType.write();
	});
	audio[4].addEventListener("ended", function(){
		timeout[2] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
			audio[5].play();
		}, 5000);
	});
	
	nextButton.fadeOut(0);
	balls.fadeOut(0);
	text.fadeOut(0);
	hint.fadeOut(0);
	periodicTable.css("height", "0%");
	periodicTable.css("opacity", "0.9");
	keepThinking.fadeOut(0);
	
	var startButtonListener = function(){
		periodicTable.addClass("transition-2s");
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		oxygenAmount -= 10;
		drawPointer();
		audio[0].volume = 0.4;
		audio[1].volume = 0.2;
		audio[1].play();
		audio[0].play();
		audio[2].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch113 = function()
{
	var theFrame = $("#frame-113"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		text = $(prefix + ".text"),
		blank = $(prefix + ".blank"),
		task = $(prefix + ".task"),
		balls = $(prefix + ".ball"), 
		checkButton = $(prefix + ".check-button"), 
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		answer = $(prefix + ".answer"),
		b1proton = 0,
		b1neutron = 0,
		b2proton = 0,
		b2neutron = 0, 
		b3proton = 0, 
		b3neutron = 0;
	
	textType = new TypingText(text, 50, true, function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			task.fadeIn(500);
			blank.fadeIn(500);
			helpButton.fadeIn(500);
			balls.fadeIn(500);
		}, 2000);
	});
	
	audio[0] = new Audio("audio/s13_1.mp3");
	audio[1] = new Audio("audio/s15_1.mp3");
	audio[2] = new Audio("audio/s15_2.mp3");
	audio[3] = new Audio("audio/s15_3.mp3");
	audio[4] = new Audio("audio/s15_4.mp3");
	
	audio[1].addEventListener("ended", function(){
		audio[2].play();
	});
	audio[2].addEventListener("ended", function(){
		audio[3].play();
		blackSkin.fadeIn(500);
		nextButton.fadeIn(500);
	});
	audio[3].addEventListener("ended", function(){
		audio[4].play();
	});
	
	//DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
	var successCondition = function(vegetable, basket)
	{
		return basket === "basket";
	}
	var successFunction = function(vegetable, basket)
	{
		
		var temp = vegetable.clone();
		temp.removeClass("box-shadow-white");
		temp.css("z-index", "2");
		$(prefix).append(temp);
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		
		if(basket.hasClass("basket-1"))
		{
			if(vegetable.hasClass("proton"))
				b1proton ++;
			else
				b1neutron ++;
		}
		
		if(basket.hasClass("basket-2"))
		{
			if(vegetable.hasClass("proton"))
				b2proton ++;
			else
				b2neutron ++;
		}
		
		if(basket.hasClass("basket-3"))
		{
			if(vegetable.hasClass("proton"))
				b3proton ++;
			else
				b3neutron ++;
		}
		
		console.log("b1proton: " + b1proton);
		console.log("b1neutron: " + b1neutron);
		console.log("b2proton: " + b2proton);
		console.log("b2neutron: " + b2neutron);
		console.log("b3proton: " + b3proton);
		console.log("b3neutron: " + b3neutron);
		
		if((b1proton || b1neutron) &&
			(b2proton || b2neutron) &&
			(b3proton || b3neutron))
			checkButton.fadeIn(500);
	}
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	}
	var finishCondition = function()
	{
	}
	var finishFunction = function()
	{
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	answer.fadeOut(0);
	blank.fadeOut(0);
	balls.fadeOut(0);
	task.fadeOut(0);
	helpButton.fadeOut(0);
	nextButton.fadeOut(0);
	text.fadeOut(0);
	checkButton.fadeOut(0);
	periodicTable.css("height", "0%");
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var checkButtonListener = function(){
		answer.fadeIn(0);
		if(b1proton === 4)
			oxygenAmount += 5;
		if(b1neutron === 5)
			oxygenAmount += 5;
		if(b2proton === 8)
			oxygenAmount += 5;
		if(b2neutron === 8)
			oxygenAmount += 5;
		if(b2proton === 12)
			oxygenAmount += 5;
		if(b2neutron === 12)
			oxygenAmount += 5;
		
		drawPointer();
		audio[1].play();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		text.fadeIn(500);
		periodicTable.addClass("transition-2s");
		textType.write();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch117 = function()
{
	var theFrame = $("#frame-117"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		text = $(prefix + ".text"),
		task = $(prefix + ".task"),
		table = $(prefix + ".table"),
		inputs = $(prefix + "input"),
		checkButton = $(prefix + ".check-button"),
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	audio[1] = new Audio("audio/s17_1.mp3");
	audio[2] = new Audio("audio/s17_2.mp3");
	audio[3] = new Audio("audio/s17_3.mp3");
	audio[4] = new Audio("audio/s17_4.mp3");
	audio[5] = new Audio("audio/s17_5.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	
	audio[1].addEventListener("ended", function(){
		audio[2].play();
	});
	audio[2].addEventListener("ended", function(){
		audio[3].play();
	});
	audio[3].addEventListener("ended", function(){
		audio[4].play();
	});
	audio[4].addEventListener("ended", function(){
		audio[5].play();
	});
	audio[5].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			task.fadeIn(500);
			taskType.write();
		}, 2000);
	});
	
	taskType = new TypingText(task, 50, true, function(){
		table.fadeIn(500);
		helpButton.fadeIn(0);
	});
	
	inputs.val("");
	
	oxygenAmount -= 10;
	drawPointer();
	nextButton.fadeOut(0);
	checkButton.fadeOut(0);
	text.fadeOut(0);
	table.fadeOut(0);
	task.fadeOut(0);
	periodicTable.css("height", "0%");
	helpButton.fadeOut(0);
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var inputListener = function(){
		var emptyNum = 14;
		for(var i = 0; i < inputs.length; i++)
			if($(inputs[i]).val())
				emptyNum --;
			
		if(!emptyNum)
			checkButton.fadeIn(500);
	};
	inputs.on("keyup", inputListener); //jquery 
	
	var checkButtonListener = function(){
		for(var i = 0; i < inputs.length; i++)
		{
			if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
			{
				oxygenAmount += 5;
				$(inputs[i]).css("color", "green");
			}
			else
			{
				$(inputs[i]).css("color", "red");
			}
			drawPointer();
		}
		
		timeout[2] = setTimeout(function(){
			checkButton.fadeOut(0);
			for(var i = 0; i < inputs.length; i++)
			{
				if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("font-weight", "bold");
					$(inputs[i]).css("font-size", "1.3vmax");
				}
				else
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("color", "normal");
					$(inputs[i]).val($(inputs[i]).attr("data-correct"));
					$(inputs[i]).css("font-size", "1.1vmax");
				}
			}
		}, 3000);
		
		timeout[3] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 8000);
		
		drawPointer();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var startButtonListener = function(){
		periodicTable.addClass("transition-2s");
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		text.fadeIn(1000);
		audio[0].volume = 0.3;
		audio[0].play();
		audio[1].play();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch121 = function()
{
	var theFrame = $("#frame-121"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".task"),
		textContainer = $(prefix + ".text-container"), 
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table");
	
	audio[0] = new Audio("audio/s21_1.mp3");
	
	audio[0].addEventListener("ended", function(){
		taskType.write();
		task.fadeIn(500);
	});
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	taskType = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		textContainer.fadeIn(500);
		helpButton.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		basket.css("color", vegetable.css("color"));
		basket.css("font-size", vegetable.css("font-size"));
		basket.css("font-style", vegetable.css("font-style"));
		vegetable.remove();
		oxygenAmount += 5;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		blackSkin.fadeIn(0);
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			blackSkin.fadeOut(0);
			keepThinking.fadeOut(0);
		}, 2000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length <= 4;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		drawPointer();
		timeout[0] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 2000);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	oxygenAmount -= 10;
	
	nextButton.fadeOut(0);
	keepThinking.fadeOut(0);
	textContainer.fadeOut(0);
	task.fadeOut(0);
	balls.fadeOut(0);
	helpButton.fadeOut(0);
	periodicTable.css("height", "0%");
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		periodicTable.addClass("transition-2s");
		audio[0].play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch124 = function()
{
	var theFrame = $("#frame-124"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		text = $(prefix + ".text"),
		task = $(prefix + ".task"),
		table = $(prefix + ".table"),
		inputs = $(prefix + "input"),
		checkButton = $(prefix + ".check-button"),
		helpButton = $(prefix + ".help-button"),
		periodicTable = $(prefix + ".periodic-table"),
		kalii = $(prefix + ".kalii"),
		natrii = $(prefix + ".natrii");
	
	audio[0] = new Audio("audio/sci-fi.mp3");
	audio[1] = new Audio("audio/s24_1.mp3");
	audio[2] = new Audio("audio/s25_1.mp3");
	audio[3] = new Audio("audio/s24_0_aliens_talking.mp3");
	audio[4] = new Audio("audio/s27_1.mp3");
	audio[5] = new Audio("audio/s27_2.mp3");
	audio[6] = new Audio("audio/s27_3.mp3");
	
	audio[0].addEventListener("ended", function(){
		this.play();
	});
	
	audio[1].addEventListener("ended", function(){
		audio[2].play();
	});
	audio[2].addEventListener("ended", function(){
		task.fadeIn(500);
		taskType.write();
	});
	
	audio[4].addEventListener("ended", function(){
		kalii.fadeIn(500);
		audio[5].play();
	});
	audio[5].addEventListener("ended", function(){
		audio[6].play();
		kalii.fadeOut(500);
		natrii.fadeIn(500);
	});
	audio[6].addEventListener("ended", function(){
		blackSkin.fadeIn(0);
		nextButton.fadeIn(0);
	});
	
	taskType = new TypingText(task, 50, true, function(){
		table.fadeIn(500);
		helpButton.fadeIn(0);
	});
	
	inputs.val("");
		
	oxygenAmount -= 10;
	drawPointer();
	nextButton.fadeOut(0);
	checkButton.fadeOut(0);
	text.fadeOut(0);
	table.fadeOut(0);
	task.fadeOut(0);
	periodicTable.css("height", "0%");
	helpButton.fadeOut(0);
	kalii.fadeOut(0);
	natrii.fadeOut(0);
	
	var helpListener = function(){
		if(parseInt(periodicTable.css("height")))
			periodicTable.css("height", "0%");
		else
			periodicTable.css("height", "100%");
	};
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var inputListener = function(){
		var emptyNum = 14;
		for(var i = 0; i < inputs.length; i++)
			if($(inputs[i]).val())
				emptyNum --;
			
		if(!emptyNum)
			checkButton.fadeIn(500);
	};
	inputs.on("keyup", inputListener); //jquery 
	
	var checkButtonListener = function(){
		for(var i = 0; i < inputs.length; i++)
		{
			if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
			{
				oxygenAmount += 5;
				$(inputs[i]).css("color", "green");
			}
			else
			{
				$(inputs[i]).css("color", "red");
			}
			drawPointer();
		}
		
		timeout[2] = setTimeout(function(){
			checkButton.fadeOut(0);
			for(var i = 0; i < inputs.length; i++)
			{
				if($(inputs[i]).val() === $(inputs[i]).attr("data-correct"))
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("font-weight", "bold");
					$(inputs[i]).css("font-size", "1.3vmax");
				}
				else
				{
					$(inputs[i]).css("color", "");
					$(inputs[i]).css("color", "normal");
					$(inputs[i]).val($(inputs[i]).attr("data-correct"));
					$(inputs[i]).css("font-size", "1.1vmax");
				}
			}
		}, 3000);
		
		timeout[3] = setTimeout(function(){
			table.fadeOut(0);
			task.fadeOut(0);
			helpButton.fadeOut(0);
			checkButton.fadeOut(0);
			audio[4].play();
		}, 8000);
		
		drawPointer();
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var startButtonListener = function(){
		periodicTable.addClass("transition-2s");
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		text.fadeIn(1000);
		audio[0].volume = 0.3;
		audio[3].volume = 0.3;
		audio[0].play();
		audio[3].play();
		audio[1].play();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch127 = function()
{
	var theFrame = $("#frame-127"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		blackSkin = $(prefix + ".black-skin"),
		astronautApproach = $(prefix + ".astronaut-approach"),
		astronautWave = $(prefix + ".astronaut-wave"),
		shipLeaving = $(prefix + ".ship-leaving"),
		menuButton = $(prefix + ".next-button");
	
	audio[0] = new Audio("audio/spacecraft-sound.mp3");
	audio[1] = new Audio("audio/s28_1.mp3");
	
	shipLeavingSprite = new Motio(shipLeaving[0], {
		"fps": "2",
		"frames": "7"
	});
	
	shipLeavingSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			blackSkin.addClass("transition-2s");
			blackSkin.css("opacity","1");
			timeout[1] = setTimeout(function(){
				menuButton.fadeIn(0);
				audio[0].pause();
			}, 2000);
		}
	});
	
	astronautApproachSprite = new Motio(astronautApproach[0], {
		"fps": "3", 
		"frames": "6"
	});
	astronautApproachSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			audio[0].play();
			astronautApproach.fadeOut(0);
			astronautWave.fadeIn(0);
			astronautWaveSprite.play();
			timeout[0] = setTimeout(function(){
				astronautWaveSprite.pause();
				astronautWave.fadeOut(0);
				theFrame.css("background-image", "url(pics/s27-bg-2.png)");
				shipLeaving.fadeIn(0);
				shipLeavingSprite.play();
			}, 3000);
		}	
	});
	
	astronautWaveSprite = new Motio(astronautWave[0], {
		"fps": "3", 
		"frames": "3"
	});
	
	astronautWave.fadeOut(0);
	menuButton.fadeOut(0);
	shipLeaving.fadeOut(0);
	
	var startButtonListener = function(){
		audio[1].play();
		startButton.fadeOut(0);
		blackSkin.css("opacity", "0");
		astronautApproachSprite.play();
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch201 = function()
{
	var theFrame = $("#frame-201"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		blackSkin = $(prefix + ".black-skin"),
		taskBG = $(prefix + ".task-bg"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".title"),
		nextButton = $(prefix + ".next-button");
		
	var typeTask = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		taskBG.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		blackSkin.fadeIn(0);
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			blackSkin.fadeOut(0);
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length == 0;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		blackSkin.fadeIn(500);
		nextButton.fadeIn(500);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	keepThinking.fadeOut(0);
	balls.fadeOut(0);
	taskBG.fadeOut(0);
	nextButton.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		taskBG.fadeIn(500);
		typeTask.write();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch202 = function()
{
	var theFrame = $("#frame-202"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		blackSkin = $(prefix + ".black-skin"),
		taskBG = $(prefix + ".task-bg"),
		balls = $(prefix + ".ball"),
		keepThinking = $(prefix + ".keep-thinking"),
		task = $(prefix + ".title"),
		nextButton = $(prefix + ".next-button"),
		table = $(prefix + ".table");
		
	var typeTask = new TypingText(task, 50, true, function(){
		balls.fadeIn(500);
		taskBG.fadeIn(500);
		table.fadeIn(500);
	});
	
	var successCondition = function(vegetable, basket)
	{
		return vegetable === basket;
	};
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		vegetable.remove();
		oxygenAmount += 10;
		drawPointer();
	};
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		blackSkin.fadeIn(0);
		keepThinking.fadeIn(0);
		timeout[1] = setTimeout(function(){
			blackSkin.fadeOut(0);
			keepThinking.fadeOut(0);
		}, 3000);
	};
	
	var finishCondition = function(vegetable, basket)
	{
		return $(prefix + ".ball").length == 0;
	};
	
	var finishFunction = function(vegetable, basket)
	{
		blackSkin.fadeIn(500);
		nextButton.fadeIn(500);
	};
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	keepThinking.fadeOut(0);
	balls.fadeOut(0);
	taskBG.fadeOut(0);
	nextButton.fadeOut(0);
	table.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		taskBG.fadeIn(500);
		typeTask.write();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch203 = function()
{
	var theFrame = $("#frame-203"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		blackSkin = $(prefix + ".black-skin"),
		nextButton = $(prefix + ".next-button"),
		downloadButton = $(prefix + ".download"),
		textarea = $(prefix + "textarea"),
		title = $(prefix + ".title");
		
	var typeTask = new TypingText(title, 100, true, function(){
		textarea.fadeIn(500);
	});
	
	var downloadButtonListener = function(){
		var blob = new Blob([textarea.val()], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "Мое Сочинение.txt");
		blackSkin.fadeIn(0);
		nextButton.fadeIn(0);
	};
	downloadButton.off("click", downloadButtonListener);
	downloadButton.on("click", downloadButtonListener);
	
	nextButton.fadeOut(0);
	textarea.val("");
	title.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		title.fadeIn(500);
		typeTask.write();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	$(".o2").fadeIn(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	if(elem.hasClass("fact"))
		$(".o2").fadeOut(0);
	
	switch(elem.attr("id"))
	{
		case "frame-000":
			$(".o2").fadeOut(0);
			launch000();
			break;
		case "frame-101":
			launch101();
			break;
		case "frame-102":
			launch102();
			break;
		case "frame-103":
			launch103();
			break;
		case "frame-104":
			launch104();
			break;
		case "frame-105":
			launch105();
			break;
		case "frame-106":
			launch106();
			break;
		case "frame-107":
			launch107();
			break;
		case "frame-108":
			launch108();
			break;
		case "frame-109":
			launch109();
			break;
		case "frame-113":
			launch113();
			break;
		case "frame-117":
			launch117();
			break;
		case "frame-121":
			launch121();
			break;
		case "frame-124":
			launch124();
			break;
		case "frame-127":
			launch127();
			break;
		case "frame-201":
			launch201();
			break;
		case "frame-202":
			launch202();
			break;
		case "frame-203":
			launch203();
			break;
	}
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	oxygenPointer = $(".o2-pointer");
	initMenuButtons();
	//hideEverythingBut($("#pre-load"));
	hideEverythingBut($("#frame-000"));
	//loadImages();
};

$(document).ready(main);